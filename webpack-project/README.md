This is an example of using blaze to render dynamic HTML with webpack.

This will likely only work in modernish browsers, as no polyfilling is done for you.

run `npm install` to install the dependencies then
run `webpack` to build the bundle.

navigate to `your/install/directory/webpack-project/index.html` and see your rendered content.
