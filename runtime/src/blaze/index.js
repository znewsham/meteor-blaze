const Blaze = require('./preamble.js');
require('./dombackend.js');
require('./domrange.js');
require('./events.js');
require('./attrs.js');
require('./materializer.js');
require('./exceptions.js');
require('./view.js');
require('./builtins.js');
require('./lookup.js');
require('./template.js');
require('./backcompat.js');

module.exports = Blaze;
