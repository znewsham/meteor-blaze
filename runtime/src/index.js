 _ = require("underscore");
Spacebars = require("../src/spacebars/spacebars-runtime.js");
Template = require("../src/templating-runtime/templating.js");
Blaze = require("../src/blaze/index.js");
Tracker = typeof Package !== "undefined" && Package.tracker ? Package.tracker.Tracker : require("meteor-standalone-tracker");
ReactiveVar = typeof Package !== "undefined" && Package["reactive-var"] ? Package["reactive-var"].ReactiveVar : require("meteor-standalone-reactive-var");
HTML = require("meteor-blaze-common").HTML;
